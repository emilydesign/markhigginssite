var listOfLinks = document.querySelectorAll('a');
for (var i = 0; i < listOfLinks.length; i++) {
  if (listOfLinks[i].id) {
    mixpanel.track_links("#" + listOfLinks[i].id, "Clicked: " + listOfLinks[i].id, {
      "referrer": document.referrer
    });
  }
}
// Track email course signups
mixpanel.track_forms("#mc-embedded-subscribe", "Signed up for QC-Course", {
    "page": document.URL
});
// Track tral signups
mixpanel.track_forms("#new-account-signup", "Signed up for trial");

$("#new-account-form").submit(function(){
    mixpanel.register({
        "company": $("#new-account-form #input_company").val(),
        "name": $("#new-account-form #input_name").val(),
        "email": $("#new-account-form #input_email").val(),
        "phone": $("#new-account-form #input_phone").val(),
        "producer_class": $("#new-account-form #input_company_class").val()
    });
});
// Register once referer
mixpanel.register_once({
    "referrer": document.referrer
});

// mixpanel.delegate_links = function (parent, selector, event_name, properties) {
//     properties = properties || {};
//     parent = parent || document.body;
//     parent = $(parent);

//     parent.on('click', selector, function (event) {
//         var new_tab = event.which === 2 || event.metaKey || event.target.target === '_blank' || this.target === '_blank';

//         properties.url = event.target.href || this.href;

//         function callback() {
//             if (new_tab) {
//                 return;
//             }

//             window.location = properties.url;
//         }

//         if (!new_tab) {
//             event.preventDefault();
//             setTimeout(callback, 300);
//         }

//         mixpanel.track(event_name, properties, callback);
//     });
// };

// mixpanel.delegate_links(document.body, 'a', "clicked link");

// function createUserFromGoldTrialRegistration() {
//   mixpanel.identify(mixpanel.get_distinct_id);
//   mixpanel.people.set({
//       "$first_name": "Joe",
//       "$last_name": "Doe",
//       "$created": "2013-04-01T09:02:00",
//       "$email": "joe.doe@example.com"
//   });
// }

// function createUserFromLandingPageRegistration() {
//   console.log("ping");
//   mixpanel.identify(mixpanel.get_distinct_id);
//   mixpanel.people.set({
//       "$name": document.getElementById('mce-MMERGE1').value,
//       "$email": document.getElementById('mce-EMAIL').value,
//       "company": document.getElementById('mce-MMERGE2').value,
//       "phone_number": document.getElementById('mce-MMERGE3').value,
//       "$created": Math.round(new Date().getTime() / 1000),
//   });
// }

// document.getElementById("startTrial").addEventListener("click", createUserFromGoldTrialRegistration);
// document.getElementById("mc-embedded-subscribe").addEventListener("click", createUserFromLandingPageRegistration);
